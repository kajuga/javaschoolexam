package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String rpnResult = rpn(statement);
        if (rpnResult != null) {
            return calculate(rpnResult);
        }
        return null;
    }

    public String calculate(String rpnResult) {
        double a, b;
        String temp;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(rpnResult);
        while (tokenizer.hasMoreTokens()) {
            try {
                temp = tokenizer.nextToken().trim();
                if (1 == temp.length() && isOperator(temp.charAt(0))) {
                    if (stack.size() < 2) {
                        return null;
                    }
                    b = stack.pop();
                    a = stack.pop();
                    switch (temp.charAt(0)) {
                        case '+':
                            a += b;
                            break;
                        case '-':
                            a -= b;
                            break;
                        case '*':
                            a *= b;
                            break;
                        case '/':
                            a /= b;
                            break;
                    }
                    stack.push(a);
                } else {
                    a = Double.parseDouble(temp);
                    stack.push(a);
                }
            } catch (Exception e) {
                return null;
            }
        }

        if (stack.size() > 1) {
            return null;
        }
        return parseCalculateResult(stack.pop());
    }

    private String parseCalculateResult(Double result){
        if (Double.isInfinite(result)){
            return null;
        }
        String strValue = String.valueOf(result);
        return strValue.endsWith(".0") ? strValue.substring(0, strValue.lastIndexOf(".0")) : strValue;
    }


    private String rpn(String input) {
        if (input == null || input.isEmpty()){
            return null;
        }
        StringBuilder sbStack = new StringBuilder(), output = new StringBuilder();
        char current, charTemp;
        for (int i = 0; i < input.length(); i++) {
            current = input.charAt(i);
            if (isOperator(current)) {
                while (sbStack.length() > 0) {
                    charTemp = sbStack.substring(sbStack.length() - 1).charAt(0);
                    if (isOperator(charTemp) && (getPriority(current) <= getPriority(charTemp))) {
                        output.append(" ").append(charTemp).append(" ");
                        sbStack.setLength(sbStack.length() - 1);
                    } else {
                        output.append(" ");
                        break;
                    }
                }
                output.append(" ");
                sbStack.append(current);
            } else if ('(' == current) {
                sbStack.append(current);
            } else if (')' == current) {
                charTemp = sbStack.substring(sbStack.length() - 1).charAt(0);
                while ('(' != charTemp && sbStack.length() > 0) {
                    /*if (sbStack.length() < 1) {
                        return null;
                    }
                    output.append(" ").append(charTemp);
                    sbStack.setLength(sbStack.length() - 1);
                    charTemp = sbStack.substring(sbStack.length() - 1).charAt(0);*/
                    /*if (sbStack.length() < 1) {
                        return null;
                    }*/
                    output.append(" ").append(charTemp);
                    sbStack.setLength(sbStack.length() - 1);
                    if (sbStack.length() > 0) {
                        charTemp = sbStack.substring(sbStack.length() - 1).charAt(0);
                    } else {
                        return null;
                    }
                }
                sbStack.setLength(sbStack.length() - 1);
            } else {
                output.append(current);
            }
        }
        while (sbStack.length() > 0) {
            output.append(" ").append(sbStack.substring(sbStack.length() - 1));
            sbStack.setLength(sbStack.length() - 1);
        }
        return output.toString();
    }

    public byte getPriority(char c) {
        switch (c) {
            case '/':
            case '*':
                return 2;
        }
        return 1;
    }

    public boolean isOperator(char c) {
        return "+-*/".indexOf(c) != -1;
    }

}
