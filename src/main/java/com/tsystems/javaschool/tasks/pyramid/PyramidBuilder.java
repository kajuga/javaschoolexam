package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || Collections.max(inputNumbers) == 0) throw new CannotBuildPyramidException();
        double d;
        int height, width;
        if ((d = (Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2) % 1 == 0) {
            Collections.sort(inputNumbers);
            height = (int) d;
            width = 2 * height - 1;
            int[][] arr = new int[height][width];
            int pointer = inputNumbers.size() - 1;
            for (int i = 0; i < arr.length; i++) {
                int j = width - 1 - i;
                int k = height - i - 1;
                while (j >= i) {
                    arr[k][j] = inputNumbers.get(pointer--);
                    if (pointer <= 0) break;
                    j -= 2;
                }
            }
            return arr;
        }
        throw new CannotBuildPyramidException();
    }
}
